package com.ecommerce.auth.server.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * UserDto .
 *
 * @author Carlos
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class UserDto {
  private String username;
  private String email;
  private String firstName;
  private String lastName;
}
