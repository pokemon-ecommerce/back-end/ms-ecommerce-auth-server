package com.ecommerce.auth.server.model;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * ErrorResponse .
 *
 * @author Carlos
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ErrorResponse {
  @Builder.Default
  private String id = UUID.randomUUID().toString();
  private String message;
}
