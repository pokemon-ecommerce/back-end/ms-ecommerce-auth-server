package com.ecommerce.auth.server.webclient.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * RestClientConfig .
 *
 * @author Carlos
 */
@Configuration
@RequiredArgsConstructor
public class RestClientConfig {
  private final KeycloakProperties keycloakProperties;

  @Bean
  WebClient keycloakWebClient() {
    return WebClient.builder()
        .baseUrl(keycloakProperties.getBaseUrl())
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
        .build();
  }

}
