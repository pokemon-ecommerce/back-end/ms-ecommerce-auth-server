package com.ecommerce.auth.server.webclient.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * UserCredential .
 *
 * @author Carlos
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class UserCredential {
  @Builder.Default
  private boolean temporary = Boolean.FALSE;
  @Builder.Default
  private String type = "password";
  private String value;
}
