package com.ecommerce.auth.server.webclient.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * KeycloakProperties .
 *
 * @author Carlos
 */
@Configuration
@ConfigurationProperties(prefix = "keycloak")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class KeycloakProperties {
  private String baseUrl;
  private String clientId;
  private String clientSecret;
  private String realm;
  private String publicKey;
}
