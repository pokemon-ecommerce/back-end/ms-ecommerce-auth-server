package com.ecommerce.auth.server.webclient;

import com.ecommerce.auth.server.exceptions.BusinnessException;
import com.ecommerce.auth.server.webclient.config.KeycloakProperties;
import com.ecommerce.auth.server.webclient.request.LoginRequest;
import com.ecommerce.auth.server.webclient.request.UserRequest;
import com.ecommerce.auth.server.webclient.response.TokenResponse;
import java.util.function.Predicate;

import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

/**
 * Application .
 *
 * @author Carlos
 */
@Repository
@RequiredArgsConstructor
public class KeycloakApi {
  private static final String REALMS_ECOMMERCE_APP_PROTOCOL_OPENID_CONNECT_TOKEN = "/realms/ecommerce-app/protocol/openid-connect/token";
  private static final String GRANT_TYPE = "grant_type";
  private static final String CLIENT_SECRET = "client_secret";
  private static final String CLIENT_ID = "client_id";
  private static final String PASSWORD = "password";
  private final WebClient keycloakWebClient;
  private final KeycloakProperties keycloakProperties;

  /**
   * login .
   *
   * @param loginRequest .
   * @return
   */
  public Mono<TokenResponse> login(final LoginRequest loginRequest) {
    return keycloakWebClient.post()
        .uri(REALMS_ECOMMERCE_APP_PROTOCOL_OPENID_CONNECT_TOKEN)
        .body(BodyInserters.fromFormData("username", loginRequest.getUsername())
            .with(PASSWORD, loginRequest.getPassword())
            .with(CLIENT_ID, keycloakProperties.getClientId())
            .with(CLIENT_SECRET, keycloakProperties.getClientSecret())
            .with(GRANT_TYPE, PASSWORD))
        .exchangeToMono(clientResponse -> get5xxError(clientResponse, TokenResponse.class))
        .log();
  }

  /**
   * token .
   *
   * @return
   */
  public Mono<TokenResponse> token() {
    return keycloakWebClient.post()
        .uri(REALMS_ECOMMERCE_APP_PROTOCOL_OPENID_CONNECT_TOKEN)
        .body(BodyInserters.fromFormData(CLIENT_ID, keycloakProperties.getClientId())
            .with(CLIENT_SECRET, keycloakProperties.getClientSecret())
            .with(GRANT_TYPE, "client_credentials"))
        .retrieve()
        .bodyToMono(TokenResponse.class).log();
  }

  /**
   * createUser .
   *
   * @param userRequest .
   * @param token       .
   * @return
   */
  public Mono<Void> createUser(final UserRequest userRequest, final String token) {
    return keycloakWebClient.post()
        .uri("/admin/realms/{realm}/users", keycloakProperties.getRealm())
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .header(HttpHeaders.AUTHORIZATION, token)
        .body(BodyInserters.fromValue(userRequest))
        .retrieve()
        .onStatus(HttpStatus::is4xxClientError, response -> Mono.error(new BusinnessException("custom error")))
        .bodyToMono(Void.class)
        .onErrorMap(Predicate.not(BusinnessException.class::isInstance),
            throwable -> new BusinnessException(throwable.getLocalizedMessage()))
        .log();
  }

  private <T> Mono<T> get5xxError(final ClientResponse clientResponse, final Class<T> type) {
    if (clientResponse.statusCode().isError()) {
      return clientResponse.createException()
          .flatMap(error -> Mono.error(new BusinnessException(error.getLocalizedMessage())));
    }
    return clientResponse.bodyToMono(type);
  }

}
