package com.ecommerce.auth.server.webclient.request;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * UserRequest .
 *
 * @author Carlos
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class UserRequest {
  private String username;
  private String email;
  private String firstName;
  private String lastName;

  @Builder.Default
  private boolean enabled = Boolean.TRUE;

  @Builder.Default
  private boolean emailVerified = Boolean.TRUE;

  private List<UserCredential> credentials;

  private List<String> groups;

}
