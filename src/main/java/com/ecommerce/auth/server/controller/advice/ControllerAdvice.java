package com.ecommerce.auth.server.controller.advice;

import com.ecommerce.auth.server.exceptions.BusinnessException;
import com.ecommerce.auth.server.model.ErrorResponse;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import reactor.core.publisher.Mono;

/**
 * ControllerAdvice .
 *
 * @author Carlos
 */
@RestControllerAdvice
public class ControllerAdvice {

  /**
   * handleBusinessException .
   * @param businnessException .
   * @return
   */
  @ExceptionHandler(value = BusinnessException.class)
  public Mono<ErrorResponse> handleBusinessException(BusinnessException businnessException) {
    return Mono.just(
        ErrorResponse.builder()
            .message(businnessException.getMessage()).build());
  }

}
