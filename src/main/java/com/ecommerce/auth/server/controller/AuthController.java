package com.ecommerce.auth.server.controller;

import com.ecommerce.auth.server.model.LoginResponse;
import com.ecommerce.auth.server.model.User;
import com.ecommerce.auth.server.model.UserDto;
import com.ecommerce.auth.server.service.impl.AuthServiceImpl;
import com.ecommerce.auth.server.webclient.request.LoginRequest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

/**
 * AuthServiceImpl .
 *
 * @author Carlos
 */
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
  private final AuthServiceImpl authService;

  @PostMapping("/login")
  public Mono<LoginResponse> login(@RequestBody final LoginRequest loginRequest) {
    return authService.login(loginRequest);
  }

  @PostMapping("/user")
  public Mono<UserDto> createUser(@RequestBody final User user) {
    return authService.createUser(user);
  }

}
