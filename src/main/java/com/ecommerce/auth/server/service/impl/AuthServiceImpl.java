package com.ecommerce.auth.server.service.impl;

import com.ecommerce.auth.server.mappers.AuthMapper;
import com.ecommerce.auth.server.model.LoginResponse;
import com.ecommerce.auth.server.model.User;
import com.ecommerce.auth.server.model.UserDto;
import com.ecommerce.auth.server.service.AuthService;
import com.ecommerce.auth.server.webclient.KeycloakApi;
import com.ecommerce.auth.server.webclient.config.KeycloakProperties;
import com.ecommerce.auth.server.webclient.request.LoginRequest;

import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;

/**
 * AuthServiceImpl .
 *
 * @author Carlos
 */
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

  private final KeycloakApi keycloakApi;
  private final KeycloakProperties keycloakProperties;

  @Override
  public Mono<LoginResponse> login(final LoginRequest loginRequest) {
    return keycloakApi.login(loginRequest)
        .map(tokenResponse -> AuthMapper
            .tokenResponseToLoginResponse(tokenResponse, keycloakProperties.getPublicKey()));
  }

  @Override
  public Mono<UserDto> createUser(final User user) {
    return keycloakApi.token()
        .map(AuthMapper::tokenResponseToString)
        .flatMap(token -> keycloakApi
            .createUser(AuthMapper.userToUserRequest(user), token)
            .thenReturn(AuthMapper.userToUserDto(user)));
  }

}
