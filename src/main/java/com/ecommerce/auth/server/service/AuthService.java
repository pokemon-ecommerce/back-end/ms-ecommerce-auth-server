package com.ecommerce.auth.server.service;

import com.ecommerce.auth.server.model.LoginResponse;
import com.ecommerce.auth.server.model.User;
import com.ecommerce.auth.server.model.UserDto;
import com.ecommerce.auth.server.webclient.request.LoginRequest;

import reactor.core.publisher.Mono;

/**
 * AuthService .
 *
 * @author Carlos
 */
public interface AuthService {
  Mono<LoginResponse> login(LoginRequest loginRequest);

  Mono<UserDto> createUser(User user);
}
