package com.ecommerce.auth.server.exceptions;

/**
 * BusinnessException .
 *
 * @author Carlos
 */
public class BusinnessException extends RuntimeException {

  public BusinnessException(Throwable cause) {
    super(cause);
  }

  public BusinnessException(String message) {
    super(message);
  }

}
