package com.ecommerce.auth.server.mappers;

import com.ecommerce.auth.server.model.LoginResponse;
import com.ecommerce.auth.server.model.User;
import com.ecommerce.auth.server.model.UserDto;
import com.ecommerce.auth.server.webclient.request.UserCredential;
import com.ecommerce.auth.server.webclient.request.UserRequest;
import com.ecommerce.auth.server.webclient.response.TokenResponse;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.bouncycastle.util.encoders.Base64;

/**
 * AuthMapper .
 *
 * @author Carlos
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class AuthMapper {

  /**
   * userToUserRequest .
   *
   * @param user .
   * @return
   */
  public static UserRequest userToUserRequest(final User user) {
    return UserRequest.builder()
        .username(user.getUsername())
        .email(user.getEmail())
        .firstName(user.getFirstName())
        .lastName(user.getLastName())
        .groups(List.of("ADMIN_USER_GROUP"))
        .credentials(
            List.of(UserCredential.builder()
                .value(user.getPassword())
                .build()))
        .build();
  }

  /**
   * userToUserDto .
   *
   * @param user .
   * @return
   */
  public static UserDto userToUserDto(final User user) {
    return UserDto.builder()
        .username(user.getUsername())
        .email(user.getEmail())
        .firstName(user.getFirstName())
        .lastName(user.getLastName())
        .build();
  }

  /**
   * tokenResponseToLoginResponse .
   *
   * @param tokenResponse .
   * @return
   */
  public static LoginResponse tokenResponseToLoginResponse(final TokenResponse tokenResponse, final String publicKey) {
    final LoginResponse.LoginResponseBuilder builder = LoginResponse.builder()
        .accessToken(tokenResponse.getAccessToken())
        .expiresIn(tokenResponse.getExpiresIn())
        .tokenType(tokenResponse.getTokenType());

    final Claims claims = Jwts.parserBuilder()
        .setSigningKey(publicKeyToRsaPublicKey(publicKey))
        .build()
        .parseClaimsJws(tokenResponse.getAccessToken())
        .getBody();

    builder.userId(claims.getSubject());
    builder.email(claims.get("email", String.class));
    builder.name(claims.get("name", String.class));

    return builder.build();
  }

  public static String tokenResponseToString(final TokenResponse tokenResponse) {
    return String.format("%s %s", tokenResponse.getTokenType(), tokenResponse.getAccessToken());
  }

  /**
   * publicKeyToRSAPublicKey .
   *
   * @param publicKey .
   * @return
   */
  public static RSAPublicKey publicKeyToRsaPublicKey(final String publicKey) {
    try {
      final byte[] decode = Base64.decode(publicKey);
      final X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(decode);
      final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
      return (RSAPublicKey) keyFactory.generatePublic(keySpecX509);
    } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
      log.error("error generate key", e.getLocalizedMessage());
      return null;
    }
  }

}
